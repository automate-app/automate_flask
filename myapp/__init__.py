import os

from datetime import timedelta

from flask import Flask, jsonify, redirect, url_for, request, session
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
# from flask_security import MongoEngineUserDatastore

from .auth import auth
from .routes import setup
from .extensions import db, jwt  # , security
from .models import User  # , Role
from .commands import add_usecase


def create_app(test_config=None):

    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object('config')
    app.config.from_pyfile('config.py')

    CORS(app, supports_credentials=True, resources=r"/*",
         origins="http://automate-frontend.herokuapp.com")

    # CORS(app, supports_credentials=True)

    #swagger config#
    SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
        app.config["SWAGGER_URL"],
        app.config["API_URL"],
        config={
            'app_name': 'AutoMate App'
        }
    )

    app.register_blueprint(
        SWAGGERUI_BLUEPRINT,
        url_prefix=app.config["SWAGGER_URL"])
    #swagger config end#

    #extensions#
    db.init_app(app)
    jwt.init_app(app)
    # user_datastore = MongoEngineUserDatastore(db, User, Role)
    # security_ctx = security.init_app(app, user_datastore)
    #extensions end#

    #blueprint#
    app.register_blueprint(setup)
    app.register_blueprint(auth)
    #blueprint end#

    #clicommands#
    app.cli.add_command(add_usecase)
    #clicommands end#

    # check if the tokens identifier is in the blacklist set
    @jwt.token_in_blacklist_loader
    def check_if_token_revoked(decrypted_token):
        from .auth import blacklist
        jti = decrypted_token['jti']
        return jti in blacklist

    # add custom data claims to tokens
    # @jwt.user_claims_loader
    # def add_claims_to_access_token(user):
    #     use_cases = user['use_cases']
    #     use_cases_data = []

    #     for case in use_cases:
    #         use_case = case.basic_info.to_mongo().to_dict()
    #         use_case['id'] = str(case.id)
    #         use_case['title'] = case.content.article_title
    #         use_case['status'] = case.status
    #         use_cases_data.append(use_case)

    #     custom_claims = {
    #         'use_cases': use_cases_data
    #     }
    #     return custom_claims

    @jwt.user_identity_loader
    def user_identity_lookup(user):
        return user['username']

    @jwt.user_loader_callback_loader
    def user_loader_callback(identity):
        user = User.objects(username=identity).get()
        if not user:
            return None
        return user

    @jwt.user_loader_error_loader
    def custom_user_loader_error(identity):
        response = jsonify({
            "msg": "User {} not found".format(identity['username'])
        })
        return response, 404

    @jwt.expired_token_loader
    def my_expired_token_callback(expired_token):
        token_type = expired_token['type']
        response = jsonify({
            'msg': 'The {} token has expired'.format(token_type)
        })
        return response, 401

    return app
