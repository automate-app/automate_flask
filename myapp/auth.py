import json
from time import mktime
from datetime import datetime, timedelta

from flask import Blueprint, jsonify, request, url_for, redirect
from flask_jwt_extended import jwt_required, create_access_token, jwt_refresh_token_required, create_refresh_token, set_access_cookies, set_refresh_cookies, unset_jwt_cookies, current_user, get_raw_jwt, get_csrf_token

from marshmallow import ValidationError

from .models import User
from .validation import UserSchema

auth = Blueprint('auth', __name__)

#storage engine for revoked tokens#
# (consider redis or postgres for production version)
blacklist = set()
#end storage engine#

# registering new user
@auth.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    schema = UserSchema()

    try:
        schema.load(data)
    except ValidationError as err:
        response = jsonify({"msg": err.messages})
        return response, 400

    if User.objects(username=data.get('username')):
        response = jsonify({"msg": "This username is already in use."})
        return response, 400
    if User.objects(email=data.get('email')):
        response = jsonify({"msg": "This email is already in use."})
        return response, 400
    else:
        user = User(**data)
        user.set_password(data['password'])
        user.save()

        response = jsonify({
            'msg': 'User account successfully created'
        })
        return response, 201


# standard login - refresh token and fresh access token returned
@auth.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    user = User.authenticate(**data)

    if not user:
        response = jsonify({
            "authenticated": False,
            "msg": "Invalid credentials"})
        return response, 401

    access_expires = timedelta(minutes=10)
    refresh_expires = timedelta(hours=1)
    access_token = create_access_token(
        identity=user, expires_delta=access_expires, fresh=True)
    refresh_token = create_refresh_token(
        identity=user, expires_delta=refresh_expires)

    # cookies saved in the browser can't seem to be accessed by heroku-deployed front-end -> workaround: csrf_access_token needs sent in the response
    csrf_access_token = get_csrf_token(access_token)

    response = jsonify({
        'logged_in_as': user.username,
        'company_name': user.company_name,
        'access_token_exp': mktime((datetime.now() + access_expires).timetuple()),
        'csrf_access_token': csrf_access_token})

    set_access_cookies(response, access_token)
    set_refresh_cookies(response, refresh_token)

    return response, 200

# fresh login (eg. for changing user settings) - only fresh access token returned (no new refresh token)
@auth.route('/fresh-login', methods=['POST'])
def fresh_login():
    data = request.get_json()
    user = User.authenticate(**data)

    if not user:
        response = jsonify({
            "authenticated": False,
            "msg": "Invalid credentials"})
        return response, 401

    access_expires = timedelta(minutes=10)
    access_token = create_access_token(
        identity=user, expires_delta=access_expires, fresh=True)

    # cookies saved in the browser can't seem to be accessed by heroku-deployed front-end -> workaround: csrf_access_token needs sent in the response
    csrf_access_token = get_csrf_token(access_token)

    response = jsonify({
        'access_token_exp': mktime((datetime.now() + access_expires).timetuple()),
        'csrf_access_token': csrf_access_token,
        'fresh': True})

    set_access_cookies(response, access_token)

    return response, 200


# endpoint for refreshing access token
@auth.route('/refresh', methods=['GET'])
@jwt_refresh_token_required
def refresh():
    access_expires = timedelta(minutes=10)
    access_token = create_access_token(
        identity=current_user, expires_delta=access_expires, fresh=False)

    # cookies saved in the browser can't seem to be accessed by heroku-deployed front-end -> workaround: csrf_access_token needs sent in the response
    csrf_access_token = get_csrf_token(access_token)

    response = jsonify({
        'access_token_exp': mktime((datetime.now() + access_expires).timetuple()),
        'csrf_access_token': csrf_access_token,
        'fresh': False
    })

    set_access_cookies(response, access_token)
    return response, 200

# endpoint for revoking access token
@auth.route('/logout', methods=['GET'])
@jwt_required
def remove_access_token():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return redirect(url_for('auth.remove_refresh_token'))


# endpoint for revoking refresh token
@auth.route('/refresh-remove', methods=['GET'])
@jwt_refresh_token_required
def remove_refresh_token():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    response = jsonify({'msg': 'Successfully logged out'})
    unset_jwt_cookies(response)
    return response, 200
