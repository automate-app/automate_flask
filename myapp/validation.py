from marshmallow import Schema, fields, validate
from base64 import b64encode


class Base64string(fields.Field):
    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None
        base64_bytes = b64encode(value)
        base64_string = base64_bytes.decode('utf-8')
        return base64_string


class UseCaseSchema(Schema):
    id = fields.String()
    images = fields.List(Base64string())
    main_image = Base64string()
    main_thumbnail = Base64string()

    class Meta:
        # fields = ('url', 'tags', 'content', 'image_urls', 'images')
        additional = ('provider', 'basic_info', 'content', 'status')


class UserSchema(Schema):
    username = fields.String(required=True, validate=[
                             validate.Regexp(
                                 regex='^.*(?=.*\d)(?=.*[a-zA-Z]).*$', error='Username must contain at least one letter and one digit'),
                             validate.Length(min=7, max=15, error='Username must be longer than 6 and shorter than 15 characters')])
    email = fields.Email(required=True)
    company_name = fields.String(required=True)
    password = fields.String(required=True, validate=[validate.Regexp(regex='^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])',
                                                                      error='Password must contain at least 1 lowercase letter, 1 uppercase letter and a special character'), validate.Length(min=8, error='Password must be at least 8 characters.')])

    class Meta:
        additional = ('status', 'use_cases')
