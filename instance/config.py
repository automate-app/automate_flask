
SECRET_KEY = '$SECRET_KEY'
CORS_HEADERS = 'Content-Type'

MONGODB_SETTINGS = {
    "db": "automate_app",
    "host": "mongodb+srv://olayway:KXvhcqmyZIn1NT83@cluster0-6uc3p.mongodb.net/automate_app?retryWrites=true&w=majority"}

JWT_CSRF_IN_COOKIES = True  # replaced by heroku env var
JWT_SECRET_KEY = '$JWT_SECRET_KEY'
JWT_TOKEN_LOCATION = ['cookies']
# JWT_IDENTITY_CLAIM = 'sub'
# Only allow JWT cookies to be sent over https
JWT_COOKIE_SECURE = False
# Send access token only to the following endpoints
# TODO switch to specific endpoints
JWT_ACCESS_COOKIE_PATH = '/'
JWT_ACCESS_CSRF_COOKIE_PATH = '/'
# JWT_ACCESS_COOKIE_PATH = '/profile'
# JWT_ACCESS_CSRF_COOKIE_PATH = '/profile'
# Send refresh token only to the following refresh endpoint
# TODO switch to specific enpoints
JWT_REFRESH_COOKIE_PATH = '/'
JWT_REFRESH_CSRF_COOKIE_PATH = '/'
# JWT_REFRESH_COOKIE_PATH = '/refresh'
# JWT_REFRESH_CSRF_COOKIE_PATH = '/refresh'
# JWT_ACCESS_CSRF_HEADER_NAME = 'X-CSRF-ACCESS-TOKEN'
# if the cookies should be session cookies (True) or persisted cookies (False)
JWT_SESSION_COOKIE = False
# enable token revoking
JWT_BLACKLIST_ENABLED = True
# what token types to check against the blacklist (defaults to ('access', 'refresh'))
# JWT_BLACKLIST_TOKEN_CHECKS = ('access')
# JWT_CSRF_METHODS = ['POST', 'PUT', 'PATCH', 'DELETE', 'GET']
