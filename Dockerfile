FROM python:3.8

WORKDIR /home/automate

RUN pip install pipenv
COPY Pipfile Pipfile
RUN pipenv lock --requirements > requirements.txt
RUN pip install -r requirements.txt

COPY . .

EXPOSE $PORT
CMD sed -i -e 's/$SECRET_KEY/'"$SECRET_KEY"'/g' ./instance/config.py
CMD sed -i -e 's/$JWT_SECRET/'"$JWT_SECRET"'/g' ./instance/config.py
CMD flask run -p $PORT -h 0.0.0.0